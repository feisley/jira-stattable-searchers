package eu.kprod.jira.searchers;

import com.atlassian.jira.issue.customfields.SortableCustomFieldSearcher;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.statistics.CustomFieldStattable;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.JiraComponentFactory;
import com.atlassian.jira.util.JiraComponentLocator;

/**
 * CascadingSelectSearcher
 * 
 * @author Marc Trey
 * 
 */
public class CascadingSelectSearcher extends com.atlassian.jira.issue.customfields.searchers.CascadingSelectSearcher implements CustomFieldStattable,
		SortableCustomFieldSearcher {

	private final OptionsManager		optionManager;
	private JiraAuthenticationContext	authenticationContext;
	private CustomFieldInputHelper		customFieldInputHelper;

	public CascadingSelectSearcher(OptionsManager optionManager, final JiraAuthenticationContext authenticationContext,
			final CustomFieldInputHelper customFieldInputHelper) {
		super(new JiraComponentLocator(), JiraComponentFactory.getInstance());
		this.optionManager = optionManager;
		this.authenticationContext = authenticationContext;
		this.customFieldInputHelper = customFieldInputHelper;
	}

	@Override
	public LuceneFieldSorter<?> getSorter(CustomField customField) {
		return new OptionNaturalSorter(optionManager, customField);
	}

	@Override
	public StatisticsMapper<?> getStatisticsMapper(CustomField customField) {
		return new CascadingSelectStatisticsMapper(customField, optionManager, authenticationContext, customFieldInputHelper);
	}

}
