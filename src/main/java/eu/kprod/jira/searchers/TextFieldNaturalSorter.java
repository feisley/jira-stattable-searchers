package eu.kprod.jira.searchers;

/**
 * Modifies TextFieldSorter to use the natural order for strings.
 *
 * @author Jozef Kotlar
 */
import java.util.Comparator;

import com.atlassian.jira.issue.statistics.TextFieldSorter;

public class TextFieldNaturalSorter extends TextFieldSorter {

	public TextFieldNaturalSorter(final String documentConstant) {
		super(documentConstant);
	}

	@Override
	public Comparator<String> getComparator() {
		return NaturalWithNullComparator.CASE_INSENSITIVE_ORDER;
	}
}
