package eu.kprod.jira.searchers;

import java.util.Comparator;

import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.SearchRequestAppender;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.query.operator.Operator;

public class ExactTextStatisticsMapper extends FreeTextStatisticsMapper {

	public ExactTextStatisticsMapper(CustomField customField, JiraAuthenticationContext authenticationContext,
			CustomFieldInputHelper customFieldInputHelper) {
		super(customField, authenticationContext, customFieldInputHelper);
	}

	@Override
	public Comparator<String> getComparator() {
		return NaturalWithNullComparator.CASE_SENSITIVE_ORDER;
	}

	/**
	 * @since v6.0
	 */
	@Override
	public SearchRequestAppender<String> getSearchRequestAppender() {
		return new SelectOptionSearchRequestAppender(customFieldInputHelper.getUniqueClauseName(authenticationContext.getUser().getDirectoryUser(),
				customField.getClauseNames().getPrimaryName(), customField.getName()), Operator.EQUALS);
	}

}