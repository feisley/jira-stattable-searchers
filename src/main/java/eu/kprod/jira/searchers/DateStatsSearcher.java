package eu.kprod.jira.searchers;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.customfields.searchers.DateRangeSearcher;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.statistics.CustomFieldStattable;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.util.JqlLocalDateSupport;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.web.action.util.CalendarLanguageUtil;
import com.atlassian.velocity.VelocityManager;

/**
 * DateTimeStatsSearcher with CustomFieldStattable
 * 
 * @author treym
 * 
 */
public class DateStatsSearcher extends DateRangeSearcher implements CustomFieldStattable {

	private JiraAuthenticationContext	authenticationContext;
	private CustomFieldInputHelper		customFieldInputHelper;

	public DateStatsSearcher(JqlOperandResolver operandResolver, JqlLocalDateSupport jqlLocalDateSupport,
			CustomFieldInputHelper customFieldInputHelper, DateTimeFormatterFactory dateTimeFormatterFactory,
			VelocityRequestContextFactory velocityRenderContext, ApplicationProperties applicationProperties, VelocityManager velocityManager,
			CalendarLanguageUtil calendarUtils, FieldVisibilityManager fieldVisibilityManager, final JiraAuthenticationContext authenticationContext) {
		super(operandResolver, jqlLocalDateSupport, customFieldInputHelper, dateTimeFormatterFactory, velocityRenderContext, applicationProperties,
				velocityManager, calendarUtils, fieldVisibilityManager);
		this.authenticationContext = authenticationContext;
		this.customFieldInputHelper = customFieldInputHelper;
	}

	@Override
	public StatisticsMapper<?> getStatisticsMapper(CustomField customField) {
		return new DateStatisticsMapper(customField, false, authenticationContext, customFieldInputHelper);
	}

}
