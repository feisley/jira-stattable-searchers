package eu.kprod.jira.searchers;

import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.jira.issue.customfields.searchers.NumberRangeSearcher;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.statistics.CustomFieldStattable;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.FieldVisibilityManager;

/**
 * Numeric Searcher
 * 
 * @author Marc Trey
 * 
 */
public class NumericSearcher extends NumberRangeSearcher implements CustomFieldStattable {

	private JiraAuthenticationContext	jiraAuthenticationContext;
	private CustomFieldInputHelper	customFieldInputHelper;

	public NumericSearcher(FieldVisibilityManager fieldVisibilityManager, JqlOperandResolver jqlOperandResolver,
			final DoubleConverter doubleConverter, CustomFieldInputHelper customFieldInputHelper,
			JiraAuthenticationContext jiraAuthenticationContext, I18nHelper.BeanFactory beanFactory) {
		super(jqlOperandResolver, doubleConverter, customFieldInputHelper, beanFactory, fieldVisibilityManager);
		this.jiraAuthenticationContext = jiraAuthenticationContext;
		this.customFieldInputHelper = customFieldInputHelper;
	}

	@Override
	public StatisticsMapper<?> getStatisticsMapper(CustomField customField) {
		return new NumericStatisticMapper(customField, jiraAuthenticationContext, customFieldInputHelper);
	}

	@Override
	public LuceneFieldSorter<?> getSorter(CustomField customField) {
		return new NumericStatisticMapper(customField, jiraAuthenticationContext, customFieldInputHelper);
	}
}
